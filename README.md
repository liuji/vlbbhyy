# VLBbHyy

for analysis VLB->bH, H->yy
(space1 and space3)
 * Generator
   ↓
 * EVNT, sim, rec, xAOD
   ↓
 * DxAOD(HIGG1D1 new tag -- for h025 e.t.c.)
   ↓
 * MxAOD(h025)
   ↓
 * NTupleCode(h025)
   ↓
 * scan NTuple
   ↓
 * Fitting!!!Statistics!!!Plots!!!

 ## sample list
 * allSamplesAndCommand.txt on "master"
 * need to be updated to h026 (minor bugs found in previous h026 data, waiting for new deviation run)

 ## Step request DxAOD
 * see example: https://its.cern.ch/jira/browse/HGAMSW-850

 ## Step request MxAOD
 * see example: https://its.cern.ch/jira/projects/HGAMSW/issues/HGAMSW-927?filter=allopenissues
 * or better to generate ourselves within HGamCore package.
 ```
 #first modify the vertex config to hardest one
 echo "modify the file HGamAnalysisFramework/data/HGamRel21.config"
 echo "change line HgammaAnalysis.UseHardestVertex:                 NO --> YES"
 read

 cd ../run
 runHGamCutflowAndMxAOD HGamTools/MxAOD.config GridDS: $inputfileDxAOD OutputDS: $outputfileMxAOD
 ```

 ## Step run the MxAOD, get NTuples
 * see Branch "NTupleCode_v2.2_DL1r_fJVT_h025"

 ## Step scan the NTuples, get mini-NTuples
 * see Branch "scan_v2.2_fJVT"

 ## Step drawing hists
 * see Branch "getHists"

 ## Step TMVA
 * see Branch "TMVAtrain"
 * need more detailed instructions

 ## Step limits setting
 * see Branch "limitSetting"
 * use TRexFitter
 * need to be uploaded
 
